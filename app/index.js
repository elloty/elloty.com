const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const mporgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const router = require('./router');

// db setup
mongoose.connect('mongodb://mongodb:27018/auth',{
  useMongoClient: true
});



// app setup
app.use(mporgan('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
router(app);

// server app
const port = process.env.PORT || 8880;
const server = http.createServer(app);
server.listen(port);
console.log('Server listening on: 8880');
