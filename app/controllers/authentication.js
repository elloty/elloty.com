const jwt = require('jwt-simple');
const path = require('path')
const config = require('../config');
const User = require('../user');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

exports.signin = function(req, res, next) {
  // User has already had their email and password
  // We just need to give them a token
  const user = req.user;
  res.send(
    {
      token: tokenForUser(user),
      user: {
        id: user.id,
        username: user.username,
        email: user.email,
        balance: user.balance,
        scope: user.scope,
      }
    }
    );
}

exports.signup = function(req, res, next) {
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;

  if (!email || !password || !username) {
    return res.status(422).send({ error: 'You must provide username, email and password'});
  }

  // See if a user with the given email exists
  User.findOne({ email: email }, function(error, existingUser) {
    if (error) { return next(error); }

    // If a user with email does exist, return an error
    if (existingUser) {
      return res.status(422).send({ error: 'Email is in use' });
    }

    User.findOne({username: username}, function(err, userExists) {
      if (err) { return next(err); }

      // If a user with email does exist, return an error
      if (userExists) {
        return res.status(422).send({ error: 'Username is in use' });
      }

      // If a user with email does NOT exist, create and save user record
      const user = new User({
        username: username,
        email: email,
        password: password
      });

      user.save(function(err, _user) {
        if (err) { return next(err); }

        // Respond to request indicating the user was created
        res.json(
          {
            token: tokenForUser(user),
            user: {
              id: _user.id,
              username: _user.username,
              email: _user.email,
              balance: _user.balance,
              scope: _user.scope,
            }
          }
        );
      });

    })

  });
}
